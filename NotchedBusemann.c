/* 

Author: Utkarsh Pathak

Program to compute the geometry of a notched Busemann intake.

Code has been used for publication: 

Babu, V. Jagadish, Utkarsh Pathak, and Krishnendu Sinha. "Comparative Analysis of Ramp-Type and Busemann Intakes for Hypersonic Air-Breathing Engine." Proceedings of the 1st National Aerospace Propulsion Conference, Kanpur. 2017.

NOTE: Compile using -lm flag for math library.

*/

#include <stdio.h>
#include <stdlib.h>
#include <math.h>

int main()
{

	// All variables have their usual meanings, as described in the relevant publication.

	float root2 = pow(2.0,0.5);
	
	float pi = 4 * atan(1.0);

	float m3, pt3_pto;

	printf("Please enter the exit mach no. ");
	scanf("%f", &m3);

	printf("Please enter the exit total pressure to inlet total pressure ratio ");
	scanf("%f", &pt3_pto);

	float m2n = m3;

	float diff;

	float temp, m2n1, m2n2;

	do
	{
		m2n1 = (2.4 * m2n * m2n)/(0.4 * m2n * m2n + 2);
		m2n2 = 2.4/(2.8 * m2n * m2n - 0.4);
		temp = pow(m2n1,3.5) * pow(m2n2,2.5);
		diff = temp - pt3_pto;
		m2n = m2n - 0.0001;
	}
	while(diff < 0.0);

	float m3n;
	m3n = pow(((0.4*m2n*m2n+2)/(2.8*m2n*m2n-0.4)),0.5);

	float thetas;
	thetas = asin(m3n/m3);

	float v3;
	v3 = pow(((0.4*m3*m3)/(2+0.4*m3*m3)),0.5);

	float ur3;
	ur3 = v3 * cos(thetas);

	float ut3;
	ut3 = -1 * v3 * sin(thetas);

	float ur2 = ur3;
	float ut2;
	ut2 = -1 * v3 * sin(thetas) * ((2.4*m2n*m2n)/(0.4*m2n*m2n+2));

	float ux2;
	ux2 = ur2 * cos(thetas) - ut2 * sin(thetas);

	float uy2;
	uy2 = ur2 * sin(thetas) + ut2 * cos(thetas);

	float delta;
	delta = atan(uy2/ux2);

	float v2;
	v2 = pow((ur2*ur2+ut2*ut2),0.5);

	float a2;
	a2 = pow((0.4*(1-ur2*ur2-ut2*ut2)/2),0.5);

	float m2;
	m2 = v2/a2;

	/* The values have been obtained for ur2, ut2, thetas etc. now the shock cone has been solved. We will now solve the Taylor-Macoll equation as a IVP*/

	float tm[3600][11];

	tm[0][0]=thetas;
	tm[0][1]=ur2;
	tm[0][2]=ut2;
	tm[0][3]=ux2;
	tm[0][4]=uy2;
	tm[0][5]=delta;
	tm[0][6]=v2;
	tm[0][7]=a2;
	tm[0][8]=m2;

	float len, rd;
	float theta, a, b, uri, thetai, uti, ri, rad;
	float duri1, duri2, duri3, duri4;
	float duti1, duti2, duti3, duti4;
	float dri1, dri2, dri3, dri4;
	float n,h;

	theta = thetas;
	rad = 1.0;
	tm[0][9] = rad * cos(thetas);
	tm[0][10] = rad * sin(thetas);

	int k;
	for (k =1; k < 3600; k++)
	{ 
		a = theta;
		b = theta + 0.05*pi/180.0;
		n = 10;
		h = (b - a)/n;

		ri = rad;
		uri = ur2;
		thetai = theta;
		uti = ut2; 

		// RK4 integration

		int m;
		for (m = 0; m < n; m++)
		{
		duri1 = h*uti;

		duti1=h*((-1*uti*uti*uri + 0.2*(1-uri*uri-uti*uti)*(uti*(cos(thetai)/sin(thetai))+2*uri))/(uti*uti -0.2*(1-uri*uri-uti*uti)));  

		dri1=h*(uri*ri/uti);

		duri2 = h*(uti + duti1/2);

		duti2=h*((-1*(uti + duti1/2)*(uti + duti1/2)*(uri+duri1/2) + 0.2*(1-(uri+duri1/2)*(uri+duri1/2)-(uti + duti1/2)*(uti + duti1/2))*((uti + duti1/2)*(cos(thetai + h/2)/sin(thetai + h/2))+2*(uri+duri1/2)))/((uti + duti1/2)*(uti + duti1/2) -0.2*(1-(uri+duri1/2)*(uri+duri1/2)-(uti + duti1/2)*(uti + duti1/2))));

		dri2=h*((uri+duri1/2)*(ri+dri1/2)/(uti + duti1/2));

		duri3 = h*(uti + duti2/2);

		duti3=h*((-1*(uti + duti2/2)*(uti + duti2/2)*(uri+duri2/2) + 0.2*(1-(uri+duri2/2)*(uri+duri2/2)-(uti + duti2/2)*(uti + duti2/2))*((uti + duti2/2)*(cos(thetai + h/2)/sin(thetai + h/2))+2*(uri+duri2/2)))/((uti + duti2/2)*(uti + duti2/2) -0.2*(1-(uri+duri2/2)*(uri+duri2/2)-(uti + duti2/2)*(uti + duti2/2)))); 

		dri3=h*((uri+duri2/2)*(ri+dri2/2)/(uti + duti2/2));

		duri4 = h*(uti + duti3);

		duti4=h*((-1*(uti + duti3)*(uti + duti3)*(uri+duri3) + 0.2*(1-(uri+duri3)*(uri+duri3)-(uti + duti3)*(uti + duti3))*((uti + duti3)*(cos(thetai + h)/sin(thetai + h))+2*(uri+duri3)))/((uti + duti3)*(uti + duti3) -0.2*(1-(uri+duri3)*(uri+duri3)-(uti + duti3)*(uti + duti3))));

		dri4=h*((uri+duri3)*(ri+dri3)/(uti + duti3));

		uri = uri + (duri1 + 2*duri2 + 2*duri3 + duri4)/6;

		uti = uti + (duti1 + 2*duti2 + 2*duti3 + duti4)/6;

		ri = ri + (dri1 + 2*dri2 + 2*dri3 + dri4)/6;

		thetai = thetai + h;
		}

		ur2 = uri;
		ut2 = uti;
		theta = thetai;
		rad = ri;

		tm[k][0]=theta;
		tm[k][1]=ur2;
		tm[k][2]=ut2;

		ux2 = ur2 * cos(theta) - ut2 * sin(theta);
		tm[k][3]=ux2;

		uy2 = ur2 * sin(theta) + ut2 * cos(theta);
		tm[k][4]=uy2;

		delta = atan(uy2/ux2);
		tm[k][5]=delta;

		v2 = pow((ur2*ur2+ut2*ut2),0.5);
		tm[k][6]=v2;

		a2 = pow((0.4*(1-ur2*ur2-ut2*ut2)/2),0.5);
		tm[k][7]=a2;

		m2 = v2/a2;
		tm[k][8]=m2;
		tm[k][9] = rad * cos(theta);
		tm[k][10] = rad * sin(theta);

		if (delta > -1*(6.0*pi/180.0))
		{
			break;
		}
	}

	printf("The freestream Mach no. is %f \n", tm[k][8]);
	printf("The area ratio is %f \n", pow(tm[k][10]/tm[0][10],2.0));

	int u = k;

	// Printing data in Tecplot format.

	FILE *fp;
	fp = fopen("geometry.dat", "w");

	if (fp == NULL)
	{
		printf("File geometry.dat could not be generated.....\n");
		return;
	}

	fprintf(fp,"VARIABLES = \"X\" \"Y\" \"Z\"\n");
	fprintf(fp,"ZONE I=%d, J=100, K=1,F=POINT\n",k+1);
	
	// Circumscribing and creating notched geometry.

	while(k >= 0)
	{
		float x, radius;
		x = tm[k][9];
		radius = tm[k][10];

		float x1, y, z;
		int h;
		float t, degt;

		for (h = 0; h < 101; h++)
		{
			t = h * 2 * pi/100;
			degt = t * 180/pi;

			if (degt >= 45.0 && degt < 135.0) 
			{ 
				y = radius/root2;
				z = y/tan(t);
				if (x < 0.0)
				{
					x1 = x/(root2*sin(t));
				}
				else
				{
					x1 = x;
				}
			}
			else if (degt >= 225.0 && degt < 315.0) 
			{ 
				y = -1*radius/root2;
				z = y/tan(t);
				if (x < 0.0)
				{
					x1 = -1*x/(root2*sin(t));
				}
				else
				{
					x1 = x;
				}
			}
			else if (degt >= 135.0 && degt < 225.0) 
			{
				z = -1*radius/root2; 
				y = z*tan(t);
				if (x < 0.0)
				{
					x1 = -1*x/(root2*cos(t));
				}
				else
				{
					x1 = x;
				}   
			}
			else  
			{
				z = 1*radius/root2; 
				y = z*tan(t);
				if (x < 0.0)
				{
					x1 = x/(root2*cos(t));;
				}
				else
				{
					x1 = x;
				}   
			}     
			fprintf(fp,"%f %f %f\n",x1,y,z);
		}  
		k = k-1; 
	}

	fclose(fp);

	printf("The geometry.dat file has been generated - for 3D figure.....\n");

	return 0;
}
