# Computation of notched Busemann intake geometry

Program to compute the geometry of a notched Busemann intake.

Code has been used for publication: 

Babu, V. Jagadish, Utkarsh Pathak, and Krishnendu Sinha. "Comparative Analysis of Ramp-Type and Busemann Intakes for Hypersonic Air-Breathing Engine." Proceedings of the 1st National Aerospace Propulsion Conference, Kanpur. 2017.

NOTE: Compile using -lm flag for math library.
